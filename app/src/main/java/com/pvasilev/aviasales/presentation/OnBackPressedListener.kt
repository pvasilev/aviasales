package com.pvasilev.aviasales.presentation

interface OnBackPressedListener {
    fun onBackPressed(): Boolean
}