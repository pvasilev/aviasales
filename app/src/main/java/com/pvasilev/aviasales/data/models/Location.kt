package com.pvasilev.aviasales.data.models

data class Location(
    val lat: Float,
    val lon: Float
)