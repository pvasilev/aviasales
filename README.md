# Aviasales task

## Summary
Create ticket search animation between two routes.
App should consist of two screens:
1. Route chooser screen
2. Map screen

## Download sample [apk](./aviasales.apk)
<img src="screenshots/sample.gif" width="25%" />

## Screenshots

<img src="screenshots/location_screen1.png" width="25%" />
<img src="screenshots/search_screen.png" width="25%" />
<img src="screenshots/location_screen2.png" width="25%" />
<img src="screenshots/map_screen.png" width="25%" />